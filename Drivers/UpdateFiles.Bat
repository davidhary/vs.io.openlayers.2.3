@echo off
setlocal
if '%my%'=='' set my=C:\My
set dst=%my%\LIBRARIES\VS\IO\OpenLayers\Drivers
goto x64

::Copies the x64 and x86 files
:: !!! Assumes a x64 bit OS host.

:x64
Echo Copying x64 files...
echo Copying to %dst%\i64
pause
xcopy %SystemRoot%\System32\DT9816.dll %dst%\i64\*.*
set src=%programfiles(x86)%\Data Translation\DeviceDrivers
xcopy "%src%"\DT9816\*.* %dst%\i64\DT9816\*.*
xcopy "%programfiles(x86)%\Data Translation\Hardware Documentation"\DT9816um.pdf %dst%\*.*

:x86
Echo Copying x86 files...
echo Copying to %dst%\ControlPanel
xcopy %SystemRoot%\SysWOW64\DTOLCPL.cpl %dst%\ControlPanel\*.*
xcopy %SystemRoot%\SysWOW64\OLDAAPI32.dll %dst%\ControlPanel\*.*
xcopy %SystemRoot%\SysWOW64\OLMEM32.dll %dst%\ControlPanel\*.*
echo Copying to %dst%\i86
xcopy %SystemRoot%\SysWOW64\DT9816.dll %dst%\i86\*.*
xcopy "%programfiles(x86)%\Data Translation\Hardware Documentation"\DT9816um.pdf %dst%\*.*
REM set src=%programfiles(x86)%\Data Translation\Win32\DeviceDrivers
REM xcopy "%src%"\DT9816\*.* %dst%\i86\DT9816\*.*
ECHO COPY 9816 DRIvERS FROM x86 SYSTEM TO:
ECHO %dst%\i86\DT9816\*.*
Echo Done.

:cleanup
Set dst=
Set src=
pause
endlocal



