''' <summary>
''' Provides added functionality to the Data Translation Open Layers
''' <see cref="OpenLayers.Base.OlBuffer">buffer</see>.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Buffer

    Inherits Global.OpenLayers.Base.OlBuffer

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New(ByVal sampleCount As Integer, ByVal subsystem As OpenLayers.Base.AnalogSubsystem)

        MyBase.new(sampleCount, subsystem)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.disposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Returns an Integer array of arrays. </summary>
    ''' <param name="channelCount">      Number of channels. </param>
    ''' <param name="samplesPerChannel"> The samples per channel. </param>
    ''' <returns> . </returns>
    Public Shared Function AllocateInteger(ByVal channelCount As Integer, ByVal samplesPerChannel As Integer) As Integer()()
        ' allocate array for readings data.
        Dim readings()() As Integer = New Integer(channelCount - 1)() {}
        For i As Integer = 0 To channelCount - 1
            readings(i) = New Integer(samplesPerChannel - 1) {}
        Next i
        Return readings
    End Function

    ''' <summary> Returns an empty Double array of arrays. </summary>
    ''' <param name="channelCount">      . </param>
    ''' <param name="samplesPerChannel"> . </param>
    ''' <returns> . </returns>
    Public Shared Function AllocateDouble(ByVal channelCount As Integer, ByVal samplesPerChannel As Integer) As Double()()
        ' allocate array for readings data.
        Dim voltages()() As Double = New Double(channelCount - 1)() {}
        For i As Integer = 0 To channelCount - 1
            voltages(i) = New Double(samplesPerChannel - 1) {}
        Next i
        Return voltages
    End Function

    ''' <summary>Retrieves double-precision data from the buffer to an array of arrays.</summary>
    ''' <param name="buffer">Specifies the buffer.</param>
    ''' <param name="channelList">Specifies the channel list.</param>
    ''' <returns>Number of valid samples.</returns>
    Public Shared Function Voltages(ByVal buffer As OpenLayers.Base.OlBuffer,
                                    ByVal channelList As OpenLayers.Base.ChannelList) As Double()()

        If buffer Is Nothing Then
            Throw New ArgumentNullException("buffer")
        End If

        If channelList Is Nothing Then
            Throw New ArgumentNullException("channelList")
        End If

        Dim data(channelList.Count - 1)() As Double
        For i As Integer = 0 To channelList.Count - 1
            data(i) = buffer.GetDataAsVolts(channelList.Item(i))
        Next
        'For Each channel As OpenLayers.Base.ChannelListEntry In channelList
        ' LogicalChannelNumber not defined data(channel.LogicalChannelNumber) = buffer.GetDataAsVolts(channel)
        ' Next

        Return data

    End Function

#End Region

#Region " METHODS "

    ''' <summary>Retrieves voltages from the buffer to an array of arrays.</summary>
    ''' <param name="channelList">Specifies the channel list.</param>
    ''' <returns>Number of valid samples.</returns>
    Public Function Voltages(ByVal channelList As OpenLayers.Base.ChannelList) As Double()()

        If channelList Is Nothing Then
            Throw New ArgumentNullException("channelList")
        End If

        Dim data(channelList.Count - 1)() As Double
        For i As Integer = 0 To channelList.Count - 1
            data(i) = MyBase.GetDataAsVolts(channelList.Item(i))
        Next
        Return data

    End Function

    ''' <summary>Retrieves readings from the buffer to an array of arrays.</summary>
    ''' <param name="channelList">Specifies the channel list.</param>
    ''' <returns>Number of valid samples.</returns>
    Public Function Readings(ByVal channelList As OpenLayers.Base.ChannelList) As UInt32()()

        If channelList Is Nothing Then
            Throw New ArgumentNullException("channelList")
        End If

        Dim data(channelList.Count - 1)() As UInt32
        For i As Integer = 0 To channelList.Count - 1
            data(i) = MyBase.GetDataAsRawUInt32(channelList.Item(i))
        Next
        Return data

    End Function

#End Region

End Class
