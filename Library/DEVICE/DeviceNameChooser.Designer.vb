<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DeviceNameChooser
    Inherits PropertyNotifyControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                For Each d As [Delegate] In Me.DevicesLocatedEvent.SafeInvocationList
                    RemoveHandler Me.DevicesLocated, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Next
                For Each d As [Delegate] In Me.DeviceSelectedEvent.SafeInvocationList
                    RemoveHandler Me.DeviceSelected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Next
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DeviceNameChooser))
        Me._FindButton = New System.Windows.Forms.Button()
        Me._NamesComboBox = New System.Windows.Forms.ComboBox()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        '_FindButton
        '
        Me._FindButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._FindButton.Image = CType(resources.GetObject("_FindButton.Image"), System.Drawing.Image)
        Me._FindButton.Location = New System.Drawing.Point(207, 1)
        Me._FindButton.Name = "_FindButton"
        Me._FindButton.Size = New System.Drawing.Size(29, 25)
        Me._FindButton.TabIndex = 3
        '
        '_NamesComboBox
        '
        Me._NamesComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._NamesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._NamesComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._NamesComboBox.Location = New System.Drawing.Point(1, 1)
        Me._NamesComboBox.Name = "_NamesComboBox"
        Me._NamesComboBox.Size = New System.Drawing.Size(205, 25)
        Me._NamesComboBox.TabIndex = 2
        '
        'DeviceNameChooser
        '
        Me.Controls.Add(Me._FindButton)
        Me.Controls.Add(Me._NamesComboBox)
        Me.Name = "DeviceNameChooser"
        Me.Size = New System.Drawing.Size(235, 28)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _FindButton As System.Windows.Forms.Button
    Private WithEvents _NamesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip

End Class
