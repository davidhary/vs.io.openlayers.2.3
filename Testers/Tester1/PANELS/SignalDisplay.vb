''' <summary> Displays collected data in a scope format. </summary>
''' <remarks> This is the display tester.  To start this module, call the Show or ShowDialog method
''' from the default instance. </remarks>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class SignalDisplay
    Inherits isr.Core.Diagnosis.FormBase

#Region " CONSTRUCTORS AND DESTRUCTORS "


    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")>
    Private Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        ' onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        onInstantiate()

        ' update the interval timer.
        Me._CreateDataTimer.Interval = 200

    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As SignalDisplay

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SignalDisplay
        SyncLock SignalDisplay.syncLocker
            If Not SignalDisplay.Instantiated Then
                SignalDisplay.instance = New SignalDisplay
            End If
            Return SignalDisplay.instance
        End SyncLock
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return Not (SignalDisplay.instance Is Nothing OrElse SignalDisplay.instance.IsDisposed)
        End Get
    End Property

    ''' <summary> Initializes additional components. </summary>
    ''' <remarks> Call this method from the class constructor if you would like to add other controls
    ''' or change anything set by the InitializeComponent method. </remarks>
    Private Sub onInstantiate()

        _Display.ChartBeginInit()
        _Display.XDataCurrentRangeMin = XYMin
        _Display.XDataCurrentRangeMax = XYMax
        _Display.XDataRangeMin = XYMin
        _Display.XDataRangeMax = XYMax
        _Display.XDataName = "Time"
        _Display.XDataUnit = "sec"
        _Display.SignalBufferLength = defaultBufferLength
        _XMinTrackBar.Value = CType(Me._Display.XDataCurrentRangeMin, Integer)
        _XMaxTrackBar.Value = CType(Me._Display.XDataCurrentRangeMax, Integer)
        sineWaveSignal = New OpenLayers.Signals.MemorySignal
        sineWaveSignal.Name = "SineWave"
        sineWaveSignal.RangeMax = XYMax
        sineWaveSignal.RangeMin = XYMin
        sineWaveSignal.CurrentRangeMax = XYMax
        sineWaveSignal.CurrentRangeMin = XYMin
        sineWaveSignal.Unit = "SineWave Unit"
        rampSignal = New OpenLayers.Signals.MemorySignal
        rampSignal.Name = "Ramp"
        rampSignal.RangeMax = XYMax
        rampSignal.RangeMin = XYMin
        rampSignal.CurrentRangeMax = XYMax
        rampSignal.CurrentRangeMin = XYMin
        rampSignal.Unit = "Ramp Unit"
        squareWaveSignal = New OpenLayers.Signals.MemorySignal
        squareWaveSignal.Name = "SquareWave"
        squareWaveSignal.RangeMax = XYMax
        squareWaveSignal.RangeMin = XYMin
        squareWaveSignal.CurrentRangeMax = XYMax
        squareWaveSignal.CurrentRangeMin = XYMin
        squareWaveSignal.Unit = "SquareWave Unit"
        _Display.ChartEndInit()

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The default buffer length. </summary>
    Private Const defaultBufferLength As Long = 100000

    ''' <summary> The x/y minimum. </summary>
    Private Const XYMin As Long = 0

    ''' <summary> The x/y maximum. </summary>
    Private Const XYMax As Long = 1000

    ''' <summary> The sine wave signal. </summary>
    Private sineWaveSignal As OpenLayers.Signals.MemorySignal

    ''' <summary> The ramp signal. </summary>
    Private rampSignal As OpenLayers.Signals.MemorySignal

    ''' <summary> The square wave signal. </summary>
    Private squareWaveSignal As OpenLayers.Signals.MemorySignal

#End Region

#Region " METHODS "

    ''' <summary> Creates signal data. </summary>
    ''' <param name="memorySignal"> The memory signal. </param>
    Private Sub CreateSignalData(ByVal memorySignal As OpenLayers.Signals.MemorySignal)
        Dim rnd As Random = New Random(DateTime.Now.Millisecond)
        Dim xOffset As Integer = rnd.Next(-20, 20)
        Select Case memorySignal.Name
            Case "SineWave"
                Dim i As Integer = 0
                While i < _Display.SignalBufferLength
                    memorySignal.Data(i) = ((Math.Sin((i + xOffset) * (Math.PI / 180) * 0.01) * 380) + 400) + rnd.Next(20)
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
            Case "Ramp"
                Dim i As Integer = 0
                While i < _Display.SignalBufferLength
                    memorySignal.Data(i) = (((i + xOffset) Mod 100) * 2) + rnd.Next(20)
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
            Case "SquareWave"
                Dim offset As Integer = rnd.Next(20)
                Dim i As Integer = 0
                While i < _Display.SignalBufferLength
                    If ((i + xOffset) Mod 100) > ((30 + 3 * 10) + offset) Then
                        memorySignal.Data(i) = 300 + rnd.Next(10)
                    Else
                        memorySignal.Data(i) = 10 + rnd.Next(10)
                    End If
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
            Case Else
                Dim i As Integer = 0
                While i < _Display.SignalBufferLength
                    memorySignal.Data(i) = i
                    System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                ' break 
        End Select
    End Sub

    ''' <summary> Event handler. Called by _SingleRadioButton for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SingleRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SingleRadioButton.CheckedChanged
        _Display.DisableRendering()
        If _SingleRadioButton.Checked Then
            _Display.BandMode = OpenLayers.Controls.BandMode.SingleBand
        Else
            _Display.BandMode = OpenLayers.Controls.BandMode.MultiBand
        End If
        _Display.EnableRendering()
        _Display.SignalUpdate()
    End Sub

    ''' <summary> Sets track bar positions dependent on current range. </summary>
    Private Sub SetTrackbarPositionsDependentOnCurrentRange()
        If _Display.Signals.Count > 0 Then
            If _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMax > CType(_YMaxTrackBar.Maximum, Double) Then
                _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMax = CType(_YMaxTrackBar.Maximum, Double)
            End If
            If _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMin < CType(_YMinTrackBar.Minimum, Double) Then
                _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMin = CType(_YMinTrackBar.Minimum, Double)
            End If
            If _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMax > _YMaxTrackBar.Maximum Then
                _YMaxTrackBar.Value = _YMaxTrackBar.Maximum
            Else
                If _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMax < _YMaxTrackBar.Minimum Then
                    _YMaxTrackBar.Value = _YMaxTrackBar.Minimum
                Else
                    _YMaxTrackBar.Value = CType(_Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMax, Integer)
                End If
            End If
            If _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMin < _YMaxTrackBar.Minimum Then
                _YMinTrackBar.Value = _YMinTrackBar.Minimum
            Else
                If _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMin > _YMaxTrackBar.Maximum Then
                    _YMinTrackBar.Value = _YMinTrackBar.Maximum
                Else
                    _YMinTrackBar.Value = CType(_Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMin, Integer)
                End If
            End If
        End If
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Event handler. Called by _createDataTimer for tick events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _createDataTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CreateDataTimer.Tick
        _Display.DisableRendering()
        Dim indexSignal As Integer = 0
        While indexSignal < _Display.Signals.Count
            CreateSignalData(_Display.Signals(indexSignal))
            System.Math.Min(System.Threading.Interlocked.Increment(indexSignal), indexSignal - 1)
        End While
        Dim start As DateTime = DateTime.Now
        _Display.EnableRendering()
        _Display.SignalUpdate()
        _Display.Refresh()

        Dim stopTime As DateTime = DateTime.Now
        _TimeLabel.Text = CStr((stopTime.Ticks - start.Ticks) / 10000) & " [ms]"
    End Sub

    ''' <summary> Event handler. Called by _XMinTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _XMinTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XMinTrackBar.Scroll
        If _Display.Signals.Count > 0 Then
            If _XMinTrackBar.Value >= _XMaxTrackBar.Value Then
                _XMinTrackBar.Value = _XMaxTrackBar.Value - 10
            End If
            _Display.XDataCurrentRangeMin = CType(_XMinTrackBar.Value, Double)
            _Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _XMaxTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _XMaxTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _XMaxTrackBar.Scroll
        If _Display.Signals.Count > 0 Then
            If _XMinTrackBar.Value >= _XMaxTrackBar.Value Then
                _XMaxTrackBar.Value = _XMinTrackBar.Value + 10
            End If
            _Display.XDataCurrentRangeMax = CType(_XMaxTrackBar.Value, Double)
            _Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _YMaxTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _YMaxTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _YMaxTrackBar.Scroll
        If _Display.Signals.Count > 0 Then
            If _YMinTrackBar.Value >= _YMaxTrackBar.Value Then
                _YMaxTrackBar.Value = _YMinTrackBar.Value + 10
            End If
            _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMax = CType(_YMaxTrackBar.Value, Double)
            _Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _YMinTrackBar for scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _YMinTrackBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _YMinTrackBar.Scroll
        If _Display.Signals.Count > 0 Then
            If _YMinTrackBar.Value >= _YMaxTrackBar.Value Then
                _YMinTrackBar.Value = _YMaxTrackBar.Value - 10
            End If
            _Display.Signals(_SignalListComboBox.SelectedIndex).CurrentRangeMin = CType(_YMinTrackBar.Value, Double)
            _Display.SignalUpdate()
        End If
    End Sub

    ''' <summary> Event handler. Called by _SignalListComboBox for selected value changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SignalListComboBox_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SignalListComboBox.SelectedValueChanged
        SetTrackbarPositionsDependentOnCurrentRange()
    End Sub

    ''' <summary> Event handler. Called by _ColorDataButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ColorDataButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ColorDataButton.Click
        If _Display.Signals.Count > 0 Then
            _ColorDialog.Color = _Display.GetCurveColor(_SignalListComboBox.SelectedIndex)
            _ColorDialog.ShowDialog()
            _Display.SetCurveColor(_SignalListComboBox.SelectedIndex, _ColorDialog.Color)
        End If
    End Sub

    ''' <summary> Event handler. Called by _ColorAxesButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ColorAxesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ColorAxesButton.Click
        _ColorDialog.Color = _Display.AxesColor
        _ColorDialog.ShowDialog()
        _Display.AxesColor = _ColorDialog.Color

    End Sub

    ''' <summary> Event handler. Called by _ColorGridsButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ColorGridsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ColorGridsButton.Click
        _ColorDialog.Color = _Display.GridColor
        _ColorDialog.ShowDialog()
        _Display.GridColor = _ColorDialog.Color

    End Sub

    ''' <summary> Event handler. Called by _AutoScaleButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _AutoScaleButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AutoScaleButton.Click
        _Display.DisableRendering()
        _Display.AutoScale = True
        SetTrackbarPositionsDependentOnCurrentRange()
        _Display.EnableRendering()
        _Display.SignalUpdate()
    End Sub

    ''' <summary> Event handler. Called by _PrintButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PrintButton.Click
        _Display.Print()
    End Sub

    ''' <summary> Event handler. Called by )_SineWaveCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SineWaveCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SineWaveCheckBox.CheckedChanged
        Dim checkBox1 As CheckBox
        checkBox1 = CType(sender, CheckBox)

        If checkBox1.Checked Then
            Me._CreateDataTimer.Enabled = False
            _Display.DisableRendering()
            _Display.Signals.Add(sineWaveSignal)
            Dim indexSignal As Integer = _SignalListComboBox.Items.Add(sineWaveSignal.Name)
            _SignalListComboBox.SelectedIndex = indexSignal
            _Display.EnableRendering()
            Me._CreateDataTimer.Enabled = True
        Else
            If _Display.Signals.Contains(sineWaveSignal) Then
                _Display.DisableRendering()
                Dim signalIndex As Integer = _Display.Signals.IndexOf(sineWaveSignal)
                _Display.Signals.Remove(sineWaveSignal)
                _SignalListComboBox.Items.RemoveAt(signalIndex)
                If Not (_SignalListComboBox.Items.Count = 0) Then
                    _SignalListComboBox.SelectedIndex = 0
                End If
                _Display.EnableRendering()
                _Display.SignalUpdate()
            End If
        End If
    End Sub

    ''' <summary> Event handler. Called by _RampCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RampCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RampCheckBox.CheckedChanged
        Dim checkBox1 As CheckBox
        checkBox1 = CType(sender, CheckBox)

        If checkBox1.Checked Then
            Me._CreateDataTimer.Enabled = False
            _Display.DisableRendering()
            _Display.Signals.Add(rampSignal)
            Dim indexSignal As Integer = _SignalListComboBox.Items.Add(rampSignal.Name)
            _SignalListComboBox.SelectedIndex = indexSignal
            _Display.EnableRendering()
            Me._CreateDataTimer.Enabled = True
        Else
            If _Display.Signals.Contains(rampSignal) Then
                _Display.DisableRendering()
                Dim signalIndex As Integer = _Display.Signals.IndexOf(rampSignal)
                _Display.Signals.Remove(rampSignal)
                _SignalListComboBox.Items.RemoveAt(signalIndex)
                If Not (_SignalListComboBox.Items.Count = 0) Then
                    _SignalListComboBox.SelectedIndex = 0
                End If
                _Display.EnableRendering()
                _Display.SignalUpdate()
            End If
        End If
    End Sub

    ''' <summary> Event handler. Called by _SquareWaveCheckBox for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SquareWaveCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SquareWaveCheckBox.CheckedChanged

        Dim checkBox1 As CheckBox
        checkBox1 = CType(sender, CheckBox)

        If checkBox1.Checked Then
            Me._CreateDataTimer.Enabled = False
            _Display.DisableRendering()
            _Display.Signals.Add(squareWaveSignal)
            Dim indexSignal As Integer = _SignalListComboBox.Items.Add(squareWaveSignal.Name)
            _SignalListComboBox.SelectedIndex = indexSignal
            _Display.EnableRendering()
            Me._CreateDataTimer.Enabled = True
        Else
            If _Display.Signals.Contains(squareWaveSignal) Then
                _Display.DisableRendering()
                Dim signalIndex As Integer = _Display.Signals.IndexOf(squareWaveSignal)
                _Display.Signals.Remove(squareWaveSignal)
                _SignalListComboBox.Items.RemoveAt(signalIndex)
                If Not (_SignalListComboBox.Items.Count = 0) Then
                    _SignalListComboBox.SelectedIndex = 0
                End If
                _Display.EnableRendering()
                _Display.SignalUpdate()
            End If
        End If
    End Sub

#End Region

End Class