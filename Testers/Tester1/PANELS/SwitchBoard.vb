''' <summary> Launches test panels. </summary>
''' <remarks> Make this the startup form. </remarks>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Switchboard
    Inherits isr.Core.Diagnosis.FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _instance As Switchboard

    ''' <summary> true to initializing definition instance. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")>
    Private Shared _initializingDefInstance As Boolean

    ''' <summary> Returns a new or existing instance of this class. </summary>
    ''' <remarks> Use this property to get an instance of this class. Returns the default instance of
    ''' this form allowing to use this form as a singleton for. </remarks>
    ''' <value> <c>A</c> new or existing instance of the class. </value>
    Public Shared ReadOnly Property Instance() As Switchboard
        Get
            If Switchboard._instance Is Nothing OrElse Switchboard._instance.IsDisposed Then
                SyncLock Switchboard.syncLocker
                    Switchboard._initializingDefInstance = True
                    Switchboard._instance = New Switchboard
                    Switchboard._initializingDefInstance = False
                End SyncLock
            End If
            Return Switchboard._instance
        End Get
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        Me.Close()
        Exit Sub
    End Sub

    ''' <summary> Opens scope display panel. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub openScopeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenScopeButton.Click
        Using ScopePanel.Instance
            ScopePanel.Instance.ShowDialog()
        End Using
    End Sub

    ''' <summary> Event handler. Called by continuousDisplayButton for click events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub continuousDisplayButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ContinuousDisplayButton.Click

        ' launch the continuous display
        Using SignalDisplay.Get
            SignalDisplay.Get.ShowDialog()
        End Using

    End Sub

    ''' <summary> Opens analog input display panel. </summary>
    ''' <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
    ''' occurs. </exception>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub launchAnalogDisplayButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LaunchAnalogDisplayButton.Click

        Try
            ' test exception trapping.
            Throw New OperationCanceledException("Test")
        Catch ex As Exception
        End Try

        Using AnalogInputDisplayPanel.Get
            AnalogInputDisplayPanel.Get.ShowDialog()
        End Using

    End Sub

    ''' <summary> Opens the Single I/O Panel. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub openSingleIOPanelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenSingleIoPanelButton.Click

        Using SingleIO.Get
            SingleIO.Get.ShowDialog()
        End Using

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs before the form is closed. </summary>
    ''' <remarks> Use this method to optionally cancel the closing of the form. Because the form is not
    ''' yet closed at this point, this is also the best place to serialize a form's visible
    ''' properties, such as size and location. Finally, dispose of any form level objects especially
    ''' those that might needs access to the form and thus should not be terminated after the form
    ''' closed. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks> Use this method for doing any final initialization right before the form is shown.
    ''' This is a good place to change the Visible and ShowInTaskbar properties to start the form as
    ''' hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SWITCHBOARD ")

            ' set tool tips
            'initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            'loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

End Class