﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.isrIOOpenLayersTester

        Public Const AssemblyTitle As String = "Open Layers Library Tester"
        Public Const AssemblyDescription As String = "Tester for the Open Layers Library"
        Public Const AssemblyProduct As String = "IO.Open.Layers.Library.Tester.2014"

    End Class

End Namespace

