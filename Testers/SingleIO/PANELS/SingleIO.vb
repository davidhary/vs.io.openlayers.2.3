﻿Imports isr.Core.Controls.ComboBoxExtensions
''' <summary> A test panel for single I/O. </summary>
''' <remarks> Demonstrates the use of an A/D subsystem and Signal Processing library to
''' continuously acquire data.  The data are acquired into recycled buffers.  Each time a buffer
''' is filled, its data is converted to voltage, windowed, transformed with the Fast Fourier
''' Transform, and converted to dB.  The calculated buffer is then graphically displayed using
''' the data plotting custom control. </remarks>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class SingleIO
    Inherits isr.Core.Diagnosis.FormBase

#Region " METHODS "

    ''' <summary> Gets or sets a value indicating whether this instance is open. </summary>
    ''' <value> <c>True</c> if this instance is open; otherwise, <c>False</c>. </value>
    Private ReadOnly Property IsOpen As Boolean
        Get
            Return Me.device IsNot Nothing
        End Get
    End Property

    ''' <summary> Gets or sets the default device. </summary>
    ''' <value> The device. </value>
    Private Property device As OpenLayers.Base.Device

    ''' <summary> Gets or sets the default board name. </summary>
    ''' <value> The name of the board. </value>
    Private Property boardName As String

    ''' <summary> Initializes the user interface and tool tips. </summary>
    ''' <remarks> Call this method from the form load method to set the user interface. </remarks>
    Private Sub initializeUserInterface()

        Me._OpenDeviceCheckBox.Enabled = True
        Me.Text = My.Application.Info.BuildDefaultCaption(": SINGLE I/O PANEL")

    End Sub

    ''' <summary> opens access to the open layers device. </summary>
    ''' <remarks> Use this method to open the driver in real or demo modes. </remarks>
    ''' <param name="deviceName"> Name of the device. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub openIO(ByVal deviceName As String)

        ' indicate that the device is not open.
        If Me.device IsNot Nothing Then
            Me.device.Dispose()
            Me.device = Nothing
        End If

        If String.IsNullOrWhiteSpace(deviceName) Then
            Me.boardMessage = "Board not connected."
            Return
        End If

        ' clear the board name.
        Me.boardName = ""

        Try

            ' get a board 
            Me.applicationMessage = "Opening I/O Board " & deviceName & "..."
            Me.boardMessage = "Opening I/O Board " & deviceName & "..."
            Me.device = OpenLayers.Base.DeviceMgr.Get.SelectDevice(deviceName)
            Me._UpdateIOButton.Enabled = Me.device IsNot Nothing
            Me._AutoUpdateCheckBox.Enabled = Me.device IsNot Nothing

            If Me.device IsNot Nothing Then

                Me.boardName = Me.device.DeviceName
                Me.applicationMessage = "Opened I/O Board " & Me.boardName
                Me.boardMessage = "Opened I/O Board " & Me.boardName
                Me.Text = My.Application.Info.BuildDefaultCaption(": " & Me.boardName & ": SINGLE I/O ")

                ' open the sub systems
                Me._DigitalInputCheckBox.Enabled = False
                If Me._digitalIn IsNot Nothing Then
                    Me._digitalIn.Dispose()
                End If
                If Me.device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.DigitalInput) > 0 Then
                    Me.applicationMessage = "Opening digital input..."
                    Me._digitalIn = New isr.IO.OL.DigitalInputSubsystem(Me.device, 0)
                    Me.applicationMessage = "Digital input opened."

                    If Me._digitalIn IsNot Nothing Then
                        Me.applicationMessage = "Configuring digital input..."
                        Me._digitalIn.Configure()
                        Me._DigitalIoGroupBox.Enabled = True
                        Me._DigitalInputCheckBox.Enabled = True
                        Me.applicationMessage = "Digital input configured."
                    Else
                        Me._DigitalIoGroupBox.Enabled = False
                        Me.applicationMessage = "Failed configuring digital input."
                    End If
                End If

                Me._DigitalOutputCheckBox.Enabled = False
                If Me._digitalOut IsNot Nothing Then
                    Me._digitalOut.Dispose()
                End If

                If Me.device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.DigitalOutput) > 0 Then
                    Me.applicationMessage = "Opening digital output..."
                    Me._digitalOut = New isr.IO.OL.DigitalOutputSubsystem(Me.device, 0)
                    Me.applicationMessage = "Digital output opened."
                    If Me._digitalOut IsNot Nothing Then
                        Me.applicationMessage = "Configuring digital output..."
                        Me._digitalOut.Configure()
                        Me._DigitalIoGroupBox.Enabled = True
                        Me._DigitalOutputCheckBox.Enabled = True
                        Me.applicationMessage = "Digital output configured."
                    Else
                        Me.applicationMessage = "Failed configuring digital output."
                    End If
                End If

                Me._AnalogInputGroupBox.Enabled = False
                If Me._analogInput IsNot Nothing Then
                    Me._analogInput.Dispose()
                End If

                If Me.device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.AnalogInput) > 0 Then
                    ' open analog to digital sub systems
                    Me.applicationMessage = "Opening analog input..."
                    Me._analogInput = New isr.IO.OL.AnalogInputSubsystem(Me.device, 0)
                    Me.applicationMessage = "analog input opened."
                    If Me._analogInput IsNot Nothing Then
                        Me.applicationMessage = "Configuring analog input..."
                        Me._AnalogInputGroupBox.Enabled = True
                        Me._analogInput.DataFlow = OpenLayers.Base.DataFlow.SingleValue
                        Me._analogInput.Config()
                        Me._InputOneChannelCombo.Enabled = True
                        Me._InputOneChannelCombo.DataSource = Me._analogInput.AvailableChannels("{0}")
                        Me._InputOneChannelCombo.SelectedIndex = Math.Min(0, _InputOneChannelCombo.Items.Count - 1)
                        Me._InputOneRangeCombo.Enabled = True
                        Me._InputOneRangeCombo.DataSource = Me._analogInput.AvailableRanges("{0}/{1}")
                        Me._InputOneRangeCombo.SelectedIndex = 0

                        Me._InputTwoChannelCombo.Enabled = True
                        Me._InputTwoChannelCombo.DataSource = Me._analogInput.AvailableChannels("{0}")
                        Me._InputTwoChannelCombo.SelectedIndex = Math.Min(1, _InputOneChannelCombo.Items.Count - 1)
                        Me._InputTwoRangeCombo.Enabled = True
                        Me._InputTwoRangeCombo.DataSource = Me._analogInput.AvailableRanges("{0}/{1}")
                        Me._InputTwoRangeCombo.SelectedIndex = 0

                        Me._InputThreeChannelCombo.Enabled = True
                        Me._InputThreeChannelCombo.DataSource = Me._analogInput.AvailableChannels("{0}")
                        Me._InputThreeChannelCombo.SelectedIndex = Math.Min(2, _InputOneChannelCombo.Items.Count - 1)
                        Me._InputThreeRangeCombo.Enabled = True
                        Me._InputThreeRangeCombo.DataSource = Me._analogInput.AvailableRanges("{0}/{1}")
                        Me._InputThreeRangeCombo.SelectedIndex = 0

                        Me._InputFourChannelCombo.Enabled = True
                        Me._InputFourChannelCombo.DataSource = Me._analogInput.AvailableChannels("{0}")
                        Me._InputFourChannelCombo.SelectedIndex = Math.Min(3, _InputOneChannelCombo.Items.Count - 1)
                        Me._InputFourRangeCombo.Enabled = True
                        Me._InputFourRangeCombo.DataSource = Me._analogInput.AvailableRanges("{0}/{1}")
                        Me._InputFourRangeCombo.SelectedIndex = 0
                        Me.applicationMessage = "Analog input configured."
                    Else
                        Me.applicationMessage = "Failed configuring analog input."
                        Me._InputOneChannelCombo.Enabled = False
                        Me._InputOneRangeCombo.Enabled = False
                        Me._InputTwoChannelCombo.Enabled = False
                        Me._InputTwoRangeCombo.Enabled = False
                        Me._InputTwoChannelCombo.Enabled = False
                        Me._InputTwoRangeCombo.Enabled = False
                        Me._InputFourChannelCombo.Enabled = False
                        Me._InputFourRangeCombo.Enabled = False
                    End If
                End If

                ' open digital to analog sub systems
                Me._AnalogOutputGroupBox.Enabled = False
                If Me._analogOutput IsNot Nothing Then
                    Me._analogOutput.Dispose()
                End If

                ' check if device has analog output capabilities
                If Me.device.GetNumSubsystemElements(OpenLayers.Base.SubsystemType.AnalogOutput) > 0 Then
                    Me.applicationMessage = "Opening analog output..."
                    Me._analogOutput = New isr.IO.OL.AnalogOutputSubsystem(Me.device, 1)
                    Me.applicationMessage = "analog output opened."
                    If Me._analogOutput IsNot Nothing Then
                        Me.applicationMessage = "Configuring analog output..."
                        Me._AnalogOutputGroupBox.Enabled = True
                        Me._analogOutput.DataFlow = OpenLayers.Base.DataFlow.SingleValue
                        Me._analogOutput.Config()
                        Me.applicationMessage = "Analog output configured."
                    Else
                        Me.applicationMessage = "Failed configuring analog output."
                    End If
                End If

                ' open counter subsystems
                If False Then
                    Me._CounterGroupBox.Enabled = True
                Else
                    Me._CounterGroupBox.Enabled = False
                End If

                ' enable the timer
                _UpdateIOTimer.Enabled = True

                Me.applicationMessage = Me.boardName & " configured."
                Me.boardMessage = Me.boardName & " opened."

            Else

                Me.boardMessage = "board not found."

                ' disable the timer
                _UpdateIOTimer.Enabled = False

                Me.Text = My.Application.Info.BuildDefaultCaption(". NO DEVICE: SINGLE I/O ")

            End If

            ' enable controls
        Catch ex As System.Exception

            If String.IsNullOrWhiteSpace(deviceName) Then
                deviceName = "unknown"
            End If
            ex.Data.Add("@isr", "Exception opening IO.")
            ex.Data.Add("@isr.DeviceName", deviceName)
            Me.showException(ex)
            My.Application.Log.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            isr.Core.Primitives.MyMessageBox.ShowDialog(Me, ex)

        End Try

    End Sub

    ''' <summary> Closes and releases the data acquisition sub systems. </summary>
    ''' <remarks> Use this method to close and release the driver. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub closeIO()

        Try

            ' disable the timer
            _UpdateIOTimer.Enabled = False
            System.Windows.Forms.Application.DoEvents()

            Try
                If Me._digitalIn Is Nothing Then
                    Me.applicationMessage = "Warning. Digital input not defined."
                Else
                    Me._digitalIn.Dispose()
                End If
            Catch ex As Exception
                Me.showException(ex)
            End Try

            Try
                If Me._digitalOut Is Nothing Then
                    Me.applicationMessage = "Warning. Digital output not defined."
                Else
                    Me._digitalOut.Dispose()
                End If
            Catch ex As Exception
                Me.showException(ex)
            End Try

            Try
                If Me._analogInput Is Nothing Then
                    Me.applicationMessage = "Warning. Analog input not defined."
                Else
                    Me._analogInput.Dispose()
                End If
            Catch ex As Exception
                Me.showException(ex)
            End Try

            Try
                If Me._analogOutput IsNot Nothing Then
                    Me._analogOutput.Dispose()
                End If
            Catch ex As Exception
                Me.showException(ex)
            End Try

            If Me.device IsNot Nothing Then
                Me.device.Dispose()
                Me.device = Nothing
                Me.applicationMessage = Me.boardName & " closed."
                Me.boardMessage = Me.boardName & " close."
            End If

        Catch ex As Exception
            Me.showException(ex)
        Finally

            ' disable all group boxes
            Me._AnalogInputGroupBox.Enabled = False
            Me._AnalogOutputGroupBox.Enabled = False
            Me._CounterGroupBox.Enabled = False
            Me._DigitalIoGroupBox.Enabled = False

        End Try

    End Sub

    ''' <summary> Terminates and disposes of class-level objects. </summary>
    ''' <remarks> Called from the form Closing method. </remarks>
    Private Sub terminateObjects()

        ' disable all group boxes
        Me._AnalogInputGroupBox.Enabled = False
        Me._AnalogOutputGroupBox.Enabled = False
        Me._CounterGroupBox.Enabled = False
        Me._DigitalIoGroupBox.Enabled = False

        ' disable the timer
        _UpdateIOTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' close the open layer devices
        Try
            If Me._digitalIn IsNot Nothing Then
                Me.applicationMessage = "Closing digital input, please wait..."
                Me._digitalIn.Dispose()
                Me.applicationMessage = "Digital input closed."
            End If
            If Me._digitalOut IsNot Nothing Then
                Me.applicationMessage = "Closing digital output, please wait..."
                Me._digitalOut.Dispose()
                Me.applicationMessage = "Digital output closed."
            End If
        Finally
        End Try

    End Sub

    ''' <summary> Updates all outputs and inputs. </summary>
    Private Sub updateIO()


        If Me._analogInput IsNot Nothing Then

            Dim channelNumber As Int16
            Dim channelGain As Double
            Dim voltage As Double

            If Me._analogOutput IsNot Nothing Then

                ' output analog voltages first so that we can use the analog output.
                Me._analogOutput.SingleVoltage(1) = Me._AnalogOutputZeroVoltageNumeric.Value
                Me._analogOutput.SingleVoltage(0) = Me._AnalogOutputOneVoltageNumeric.Value

            End If

            If Me._DigitalOutputCheckBox.Checked AndAlso Me._digitalOut IsNot Nothing Then
                Me._digitalOut.SingleReading = Convert.ToInt32(Me._DigitalOutputNumericUpDown.Value, Globalization.CultureInfo.CurrentCulture)
            End If

            If Me._analogInput IsNot Nothing Then

                If Int16.TryParse(Me._InputOneChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelNumber = Int16.Parse(Me._InputOneChannelCombo.Text, Globalization.CultureInfo.CurrentCulture)
                    channelGain = Me._analogInput.SupportedGains()(Convert.ToInt16(Me._InputOneRangeCombo.SelectedIndex))
                    voltage = Me._analogInput.SingleVoltage(channelNumber, channelGain)
                    Me._InputOneVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

                If Int16.TryParse(Me._InputTwoChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelGain = Me._analogInput.SupportedGains()(Convert.ToInt16(Me._InputTwoRangeCombo.SelectedIndex))
                    voltage = Me._analogInput.SingleVoltage(channelNumber, channelGain)
                    Me._InputTwoVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

                If Int16.TryParse(Me._InputThreeChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelGain = Me._analogInput.SupportedGains()(Convert.ToInt16(Me._InputThreeRangeCombo.SelectedIndex))
                    voltage = Me._analogInput.SingleVoltage(channelNumber, channelGain)
                    Me._InputThreeVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

                If Int16.TryParse(Me._InputFourChannelCombo.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, channelNumber) Then
                    channelGain = Me._analogInput.SupportedGains()(Convert.ToInt16(Me._InputFourRangeCombo.SelectedIndex))
                    voltage = Me._analogInput.SingleVoltage(channelNumber, channelGain)
                    Me._InputFourVoltageTextBox.Text = voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
                End If

            End If

        End If

        If Me._DigitalInputCheckBox.Checked AndAlso Me._digitalIn IsNot Nothing Then
            Me._DigitalInputNumericUpDown.Value = Convert.ToDecimal(Me._digitalIn.SingleReading, Globalization.CultureInfo.CurrentCulture)
        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets reference to the digital input subsystem.</summary>
    Private WithEvents _digitalIn As isr.IO.OL.DigitalInputSubsystem

    ''' <summary>Gets or sets reference to the digital output subsystem.</summary>
    Private WithEvents _digitalOut As isr.IO.OL.DigitalOutputSubsystem

    ''' <summary>Gets or sets reference to the analog input subsystem.</summary>
    Private WithEvents _analogInput As isr.IO.OL.AnalogInputSubsystem

    ''' <summary>Gets or sets reference to the analog output subsystem.</summary>
    Private WithEvents _analogOutput As isr.IO.OL.AnalogOutputSubsystem

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> The status message. </value>
    Private Property applicationMessage As String
        Get
            Return Me._ApplicationMessagesComboBox.Text
        End Get
        Set(value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If Me._ApplicationMessagesComboBox.Items.Count > 50 Then
                    Do Until Me._ApplicationMessagesComboBox.Items.Count < 25
                        Me._ApplicationMessagesComboBox.Items.RemoveAt(Me._ApplicationMessagesComboBox.Items.Count - 1)
                    Loop
                End If
                Me._ApplicationMessagesComboBox.Items.Insert(0, value)
                If Me._ApplicationMessagesComboBox.SelectedIndex <> 0 Then
                    Me._ApplicationMessagesComboBox.SelectedIndex = 0
                End If
                Me._ApplicationMessagesComboBox.Invalidate()
                Me._ToolTip.SetToolTip(Me._ApplicationMessagesComboBox, value)
            End If
        End Set
    End Property

    ''' <summary> Shows the exception. </summary>
    ''' <param name="ex"> The ex. </param>
    Private Sub showException(ByVal ex As Exception)
        Me.applicationMessage = ex.Message
        If ex.InnerException IsNot Nothing Then
            Me.boardMessage = ex.InnerException.Message
        Else
            Me.boardMessage = ex.ToString
        End If
        Me._ToolTip.SetToolTip(Me._BoardMessagesComboBox, ex.ToString)
    End Sub

    ''' <summary> Gets or sets the board error message. </summary>
    ''' <value> The board error message. </value>
    Private Property boardMessage As String
        Get
            Return Me._BoardMessagesComboBox.Text
        End Get
        Set(value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If Me._BoardMessagesComboBox.Items.Count > 50 Then
                    Do Until Me._BoardMessagesComboBox.Items.Count < 25
                        Me._BoardMessagesComboBox.Items.RemoveAt(Me._BoardMessagesComboBox.Items.Count - 1)
                    Loop
                End If
                Me._BoardMessagesComboBox.Items.Insert(0, value)
                Me._ToolTip.SetToolTip(Me._BoardMessagesComboBox, value)
                If Me._BoardMessagesComboBox.SelectedIndex <> 0 Then
                    Me._BoardMessagesComboBox.SelectedIndex = 0
                End If
                Me._BoardMessagesComboBox.Invalidate()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sync locker.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As SingleIO

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SingleIO
        If Not SingleIO.Instantiated Then
            SyncLock SingleIO.syncLocker
                SingleIO.instance = New SingleIO
            End SyncLock
        End If
        Return SingleIO.instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock SingleIO.syncLocker
                Return Not (SingleIO.instance Is Nothing OrElse SingleIO.instance.IsDisposed)
            End SyncLock
        End Get
    End Property

#End Region

#Region " ON EVENT HANDLERS "

    ''' <summary> Cleans up managed components. </summary>
    ''' <remarks> Use this method to reclaim managed resources used by this class. </remarks>
    Private Sub onDisposeManagedResources()

        If Me._digitalIn IsNot Nothing Then
            Me._digitalIn.Dispose()
            Me._digitalIn = Nothing
        End If

        If Me._digitalOut IsNot Nothing Then
            Me._digitalOut.Dispose()
            Me._digitalOut = Nothing
        End If

        If Me._analogInput IsNot Nothing Then
            Me._analogInput.Dispose()
            Me._analogInput = Nothing
        End If

        If Me._analogOutput IsNot Nothing Then
            Me._analogOutput.Dispose()
            Me._analogOutput = Nothing
        End If

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs before the form is closed. </summary>
    ''' <remarks> Use this method to optionally cancel the closing of the form. Because the form is not
    ''' yet closed at this point, this is also the best place to serialize a form's visible
    ''' properties, such as size and location. Finally, dispose of any form level objects especially
    ''' those that might needs access to the form and thus should not be terminated after the form
    ''' closed. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        ' terminate form objects
        Me.terminateObjects()

    End Sub

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks> Use this method for doing any final initialization right before the form is shown.
    ''' This is a good place to change the Visible and ShowInTaskbar properties to start the form as
    ''' hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SINGLE I/O ")

            ' set tool tips
            Me.initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by channelSelect for key press events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Key press event information. </param>
    Private Sub channelSelect_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _InputOneChannelCombo.KeyPress,
                                                                                    _InputTwoChannelCombo.KeyPress,
                                                                                    _InputThreeChannelCombo.KeyPress,
                                                                                    _InputFourChannelCombo.KeyPress
        CType(sender, ComboBox).SearchAndSelect(e)
    End Sub

    ''' <summary> Handles the CheckedChanged event of the openDeviceCheckBox control. Opens or closes
    ''' the device. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs"/> instance containing the event data. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub openDeviceCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenDeviceCheckBox.CheckedChanged

        If Not _OpenDeviceCheckBox.Enabled Then Return
        Dim isOpening As Boolean = Me._OpenDeviceCheckBox.Checked
        Try
            _OpenDeviceCheckBox.Enabled = False
            If isOpening Then
                ' open the board
                Me.openIO(DeviceChooser.SelectDeviceName())
            Else
                ' close the board
                Me.closeIO()
            End If
            Me._OpenDeviceCheckBox.Checked = Me.IsOpen

            If Me._OpenDeviceCheckBox.Checked Then
                ' set the caption to close
                Me._OpenDeviceCheckBox.Text = "Cl&ose Device"
            Else
                ' set the caption to open
                Me._OpenDeviceCheckBox.Text = "&Open Device"
            End If
        Catch ex As Exception

            If String.IsNullOrWhiteSpace(Me.boardName) Then
                Try
                    Me.boardName = DeviceChooser.SelectDeviceName()
                Catch
                End Try
            End If
            If String.IsNullOrWhiteSpace(Me.boardName) Then
                Me.boardName = "N/A"
            End If
            If isOpening Then
                Me.applicationMessage = "Failed opening " & Me.boardName & " board"
            Else
                Me.applicationMessage = "Failed closing " & Me.boardName & " board"
            End If
            ex.Data.Add("@isr", Me.applicationMessage)
            ex.Data.Add("@isr.BoardName", Me.boardName)
            Me.showException(ex)
            My.Application.Log.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            Dim box As New isr.Core.Primitives.MyMessageBox(ex)
            box.ShowDialog(Me)
        Finally
            _OpenDeviceCheckBox.Enabled = True
        End Try
    End Sub

    ''' <summary> Occurs upon timer events. </summary>
    ''' <remarks> Use this method to execute all timer actions. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _UpdateIOTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UpdateIOTimer.Tick

        If Me._AutoUpdateCheckBox.Checked Then

            Try

                Me._UpdateIOTimer.Enabled = False

                Me.updateIO()

                Me._UpdateIOTimer.Enabled = True

            Catch ex As System.Exception

                Me.applicationMessage = "Failed monitoring. Timer is stopped. Close and reopen the board."
                Me.showException(ex)

            End Try

        End If

    End Sub

    ''' <summary> Handles the CheckedChanged event of the autoUpdateCheckBox control. Toggles the
    ''' visibility of the manual update box. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs"/> instance containing the event data. </param>
    Private Sub autoUpdateCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AutoUpdateCheckBox.CheckedChanged
        Me._UpdateIOButton.Visible = Not _AutoUpdateCheckBox.Checked
    End Sub

    ''' <summary> Event handler. Called by UpdateIOButton for click events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UpdateIOButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UpdateIOButton.Click

        Try

            Me.updateIO()

        Catch ex As System.Exception

            Me.showException(ex)

        End Try

    End Sub

#End Region

End Class